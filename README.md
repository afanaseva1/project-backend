## Run using docker-compose (create network only for the first time run):

```bash
docker network create project-net
docker-compose up -d
docker-compose run --rm api python manage.py makemigrations
docker-compose run --rm api python manage.py migrate
```

## Run tests with coverage

```bash
docker-compose run --rm api sh -c "coverage run -m py.test --cache-clear && coverage report"
```

## Run flake8 analyzer

```bash
docker-compose run --rm api sh -c "flake8"
```

## Shortcuts

```bash
export COMPOSE="docker-compose"
export RUN="docker-compose run --rm api"
export MANAGE="docker-compose run --rm api python manage.py"
```
