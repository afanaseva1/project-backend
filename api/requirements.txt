django==2.0.3
django-filter==1.1.0
djangorestframework==3.8.2
drf-yasg[validation]==1.10.2
psycopg2==2.7.1
uwsgi==2.0.17
