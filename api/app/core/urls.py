from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework.routers import DefaultRouter

from core import settings
from example.views import ExampleViewSet

router = DefaultRouter(trailing_slash=True)
router.register('example', ExampleViewSet, base_name='example')

docs_schema_view = get_schema_view(
    openapi.Info(
        title='Кубышка API',
        default_version='v1',
        description='N.B. documentation is auto-generated, so some sections may be wrong. '
                    'Ask developers if you have any issues.',
        terms_of_service='',
        contact=openapi.Contact(name='Aleksey', email='alex2304el@gmail.com'),
        license=openapi.License(name='BSD License'),
    ),
    validators=['flex', 'ssv'],
    public=True,
    permission_classes=(permissions.AllowAny,),
)
docs_urls = [
    path('swagger.json', docs_schema_view.without_ui(cache_timeout=0), name='schema-swagger-json'),
    path('swagger/', docs_schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', docs_schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc-ui'),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('docs/', include(docs_urls)),
    path('api/v1/', include(router.urls))
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
